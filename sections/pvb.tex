\section{Poincaré-Van den Bergh duality}

\begin{numtext}
  In the topological world, we have a relation between the homology and cohomology of an $n$-dimensional, orientable closed manifold $M$ over a field.
  Namely, if we write $\hlgy_i(-,k),\hlgy^i(-,k)$ for the singular homology respectively cohomology with coefficients in a field $k$ or the integers, then
  \[
  \hlgy_i(M;R)\cong \hlgy_{n-i}(M;R)
  \]
  holds.
  This isomophism depends on some choice of orientation, which is reflected by the choice of a generator of
  \[
  \hlgy^n(M;R)\cong R.
  \]
  We now want to see if we can find something similar for Hochschild (co)homology.
  %TODO: put more here
\end{numtext}
\begin{lem}
  Let $\shf$ be a locally free sheaf of rank $n$ of $\ox$-modules.
  Then the \emph{multiplication map}
  \[
  \exterior^r\shf
  \tensor
  \exterior^{n-r}\shf
  \longrightarrow
  \exterior^n \shf
  \]
  is a \emph{perfect pairing}, i.e. it induces an isomophism
  \[
  \dual{\left(\exterior^{n-r}\shf\right)}{}
  \tensor
  \exterior^n
  \shf
  \isomorphism
  \exterior^r\shf.
  \]
\end{lem}
\begin{example}
  Let $A=k[x_1,\ldots,x_n]$.
  By the (affine) HKR-theorem, we have
  \[
  \hh^i(A)
  \cong
  \exterior^i \der_k(A)
  \text{ and }
  \hh_{n-1}(A)
  \cong
  \Omega^{n-1}_{A/k}.
  \]
  We immediately observe that $\hh^n(A) \cong A$.

\end{example}
\begin{nonumconvention}
  We're reentering the realm of noncommutative algebra --- if we say that $A$ is a $k$-algebra, we're still requiring it to be associative, but don't impose any commutativity conditions (unless when they're explicitly stated).
\end{nonumconvention}
\begin{defn}
  Let $U$ be a right $A^e$-module.
  We say that $U$ is \emph{invertible} if the functor
  \[
  \indmodule[A]{U}[A]\tensor_A-\mc
  A\modcat\to A\modcat
  \]
  is an auto-equivalence of the category $A\modcat$ of left $A$-modules.
\end{defn}
\begin{defn}
  Let $A$ be a $k$-algebra, and $M$ an $A$-module.
  \begin{enumerate}
  \item
  We say that $M$ has \emph{finite projective dimension} if it has a finite length resolution by projective $A$-modules.
  In that case, the minimal lenght of such a resolution is called the \emph{projective dimension} of $M$ and is denoted by $\projdim_A(M)$.
  \glsadd{projective_dimension}
  \item
  We say that $A$ is \emph{homologically smooth} if
  \begin{enumerate}
    \item its projective dimension $\projdim_{A^e}(A)$ as an $A^e$-module is finite; and
    \item there is a resolution of $A$ as an $A^e$-module consisting of finitely many, finitely generated $A^e$-modules.
  \end{enumerate}
\end{enumerate}
\end{defn}
\begin{theorem}[van den Bergh duality]
  Let $A$ be an associative $k$-algebra such that there is $d\in \nn$ and an invertible right $A^e$-module $U$ satisfying
  \[
  \hh^i(A,A^e)
  \cong
  \begin{cases}
    U
    &
    \text{if }i\neq d;
    \\
    0
    &\text{otherwise.}
  \end{cases}
  \]
  Then for every $A$-bimodule $N$, it holds that:
  \[
  \hh^i(A,N)
  \cong
  \hh_{d-i}(A,U\tensor_A N).
  \]
\end{theorem}
\begin{proof}
  The ``dualization'' that's happening here is hidden in some kind of ``derived $\hom$-$\tensor$-duality'':
  \begin{fact}
    Assume that $A$ is homologically smooth and let $N$ be an $A^e$-module.
    Then there is a natural isomophism
    \begin{equation}\label{pvb:derived-tensor-dual}
    \rderived\hom_{A^e}(A,N)
    \cong
    \rderived\hom_{A^e}(A,A^e)\dtensor_{A^e}N
    \end{equation}
    in the derived category $\derivedd(A^e)$.
  \end{fact}
  \begin{claimproof}
    something something deriving adjunctions.
  \end{claimproof}
    We also need the following compatibility statement for tensor products:
  \begin{fact}
    Let $P$ be a projective $A^e$-module.
    Then there is an isomophism of vector spaces
    \begin{equation}\label{pvb:tensor-nightmare}
    \left[\indmodule{U}[A^e]\right]\tensor_{A^e}\left[\indmodule[A^e]{P}\right]
    \cong
    \left[\indmodule{A}[A^e]\right]
    \tensor_{A^e}
    \left[\indmodule[A]{\left(\indmodule[A]{U}[A]\tensor_A \indmodule[A]{P}[A]\right)}[A]\right].
    \end{equation}
  \end{fact}
Equipped with this, the rest of this proof reduces to a calculation in the derived category:
  \begin{align*}
    \rderived \hom_{A^e}(A,N)
    &
    \cong
    \rderived \hom_{A^e}(A,A^e)\dtensor_{A^e}N
    &
    \eqref{pvb:derived-tensor-dual}
    \\
    &
    \cong
    U[-d]\dtensor_{A^e}N
    &
    (\rderived\hom_{A^e}(A,A^e)\cong U)
    \\
    &
    \cong
    A\dtensor_{A^e}\left(U\dtensor_A N\right)[-d]
    &
    \eqref{pvb:tensor-nightmare}
    \\
    &
    \cong
    A\dtensor_{A^e}\left(U\tensor_A N\right)[-d]
    &
    \text{(}U\tensor_A - \text{ is exact.)}
  \end{align*}
  The claim follows if we identify the first chain of these isomorphisms with the Hochschild cohomology of $N$ as follows:
  \[
  \hh^i(A;N)
  \cong
  \ext_{A^e}^i(A,N)
  =
  \hlgy^i(\rderived\hom_{A^e}(A,N))
  \]
  and the Hochschild homology (including the shift) as:
  \[
  \hh_{i-d}(A;U\tensor_A N)
  \cong
  \tor^{A^e}_{i-d}
  (A,U\tensor_A N)
  =
  \hlgy^{i}(A\dtensor_{A^e}(U\tensor_A N)[-d]).
  \]
\end{proof}
\begin{numtext}
  We now want to further analyse the commutative case --- so let $A$ be a commutative $k$-algebra that is smooth in the sense of \cref{hkr_aff:smooth-original-defn}.
  Assume that $A$ has finite Krull dimension $d$.
  We claim that $\hh^i(A,A^e)$ is concentrated in degree $d$:
  From the étale base change result \cref{hkr_aff:etale-base-change}, we deduce that

\end{numtext}

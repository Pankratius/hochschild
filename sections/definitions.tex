\section{Hochschild (Co)homology for algebras -- Defintions}
\begin{nonumnotation}
  Let $k$ be a field (of arbitrary characteristic, for now), and $A$ an associative $k$-algebra. As a remark, when we hear algebras, we want to think of them usually of commutative algebras or finite-dimensional (non-commutative) algebras.
\end{nonumnotation}
\begin{defn}
  The \emph{bar complex} of $A$ is defined as
  \[
  \cbarb(A)\defined
  \ldots
  \xrightarrow{d_2}
  A\ktensor A\ktensor A
  \xrightarrow{d_1}
  A\ktensor A
  \to
  0,
  \]
  \glsadd{barcomplex}
  i.e. $\cbar{n}(A) = A^{\tensor n+2}$, with differentials given on elementary tensors by
  \[
  d_n(a_0\tensor \ldots \tensor a_{n+1})\defined
  \sum_{i=0}^n
  \left(-1\right)^i
  a_o\tensor \ldots \tensor a_{i-1}\tensor (a_ia_{i+1})\tensor a_{i+2}\tensor a_n,
  \]
  i.e. the alternating sum over all possible ways of contracting consecutive elements.
\end{defn}
\begin{rem}
  We have that $A^{\tensor n}$ is an $A$-bimodule, by setting
  \[
  a\cdot (a_1\tensor \ldots \tensor a_n)\defined (aa_1)\tensor \ldots \tensor a_n
  \text{ and }
  (a_1\tensor \ldots \tensor a_n)b \defined a_1\tensor \ldots \tensor (a_nb).
  \]
  Under this structure, the differential from above is indeed $A$-bilinear.
\end{rem}
\begin{prop}\label{definitions:bar-complex-is-free-res}
  The bar complex is a free resolution of $A$ as an $A$-bimodule, via the map
  \[
  d_0\mc A\ktensor A \to A,~a_0\tensor a_1\mapsto a_0a_1.
  \]
\end{prop}
Instead of proving this all at once, we split it up into several steps, which will also help us to understand the bar complex better (and introduces us to calculations to which we better become used to early on):
\begin{lem}
  The bar complex $\cbarb(A)$ is contractible, via maps
  \[
    s_n\mc \cbar{n}(A)\to \cbar{n+1}\mt
    a_0\tensor\ldots a\np \mapsto 1\tensor a_0\tensor \ldots \tensor a\np.
  \]
  Furthermore, the image of $s_n$ generates $A^{\tensor n+3}$ as a left $A$-module.
\end{lem}
\begin{proof}
  To be a contractible chain complex means, by definition, that
  \[
  d\np \circ s_n + s\nm = \id_{\cbar{n}(A)}
  \]
  holds in the diagrams
  \[
  \begin{tikzcd}
    \ldots
    \ar{r}
    &
    \cbar{n+1}(A)
    \ar[equal]{d}
    \ar{r}[above]{d\np}
    &
    \cbar{n}(A)
    \ar[equal]{d}
    \ar{r}[above]{d_n}
    \ar{ld}[above left]{s_n}
    &
    \cbar{n-1}(A)
    \ar[equal]{d}
    \ar{ld}[below right]{s\nm}
    \ar{r}
    &
    \ldots
    \\
    \ldots
    \ar{r}
    &
    A^{\tensor n+3}
    \ar{r}[below]{d\np}
    &
    A^{\tensor n+2}
    \ar{r}[below]{d_n}
    &
    A^{\tensor n+1}
    \ar{r}
    &
    \ldots
  \end{tikzcd},
  \]
  where we already filled in the definition of the modules of the bar complex in the lower row. This calculation is a ``pleasant'' exercise -- we have:
  \begin{align*}
    d\np \circ s_n
    \left(a_0\tensor \ldots a_{n+1}\right)
    &=
    d\np\left(1\tensor a_0 \tensor \ldots \tensor a\np\right)
    \\
    &=
    a_0\tensor \ldots \tensor a\np\\
    &
    +\sum_{i=1}^{n+1}\left(-1\right)^i1\tensor a_0\tensor \ldots \tensor a_{i-1}a_i\tensor\ldots\tensor a\np
    \intertext{as well as}
    s\nm\circ d_n\left(a_0\tensor\ldots\tensor a\np\right)
    &=
    \sum_{i=0}^n(-1)^i\tensor a_0\tensor \ldots \tensor a_ia_{i+1}\tensor\ldots a\np.
  \end{align*}
  Note now that the terms in the sums agree, but with opposing signs (the one sums starts at $i=0$, the other at $i=1$)! So we're left with the term $a_0\tensor\ldots\tensor a\np$, and we're done.
\end{proof}
\begin{lem}
  The bar complex is indeed a chain complex.
\end{lem}
\begin{proof}
  We will show this by induction on $n$: In the case $n=1$, we have:
  \begin{align*}
    d_0\circ d_1\left(a_0\tensor a_1\tensor a_2\right)
    &=
    d_0\left(\left(a_0a_1\right)\tensor a_2 - a_0\tensor\left(a_1a_2\right)\right)\\
    &=
    \left(a_0a_1\right)a_2 -
    a_0\left(a_1a_2\right)\\
    &= 0,
  \end{align*}
  since $A$ is associative. For $n\geq 2$, we use the contracting homotopies from above, to first obtain
  \begin{align*}
    d_n\circ d\np s_n
    &=
    d_n - d_n\circ s\nm \circ d_n
    \\
    &= d_n - d_n + s_n\circ d\nm \circ d_n
    \\
    &=
    0,
  \end{align*}
  where the last term vanishes by induction.
\end{proof}
Summarizing, we obtain a
\begin{proof}[Proof of \cref{definitions:bar-complex-is-free-res}]
  We have just shown that $\cbarb(A)$ is indeed a complex, and since it is contrabtible, it is in particular exact.
  We also have that $\coker d_1 \cong A$, via the map $a\mapsto 1\tensor a$.\par
  For the bar complex to be a free resolution we first note that as $A$-bimodules,
  \[
  A^{\tensor n+2} \cong
  A^e\ktensor A^{\tensor n}.
  \]
  But this is indeed a free $A$-bimodule. %TODO: why?
\end{proof}
\begin{construction}
  Let $M$ be an $A$-bimodule.
  \begin{enumerate}
  \item
  The \emph{Hochschild cochain complex} $\hohccomp(A,M)$associated to $M$ is defined compenent-wise as
  \[
  \hohcompg^n(A,M)\defined
  \hom_{A^e}(\cbar{n}(A),M)
  \]
  The differential is given by $\hom_{A^e}(d,\id_M)$.
  Dually, the \emph{Hochschild chain complex} $\hohcomp(A,M)$ associated to $M$ is defined component-wise by
  \[
  \hohcompg_n(A,M) \defined M\tensor_{A^e}\cbarb(A),
  \]
  with differentials given by $\id_M\tensor d$. Both of these are $k$-vector spaces.
  \item
  Introducing the main players of the game, the \emph{Hochschild cohomology} of $M$ is defined as the homology of the Hochschild cochain complex:
  \[
  \hh^n(A,M)\defined \hlgy^n\hohccomp(A,M);
  \]
  the \emph{Hochschild homology} of $M$ is defined as the homology of the Hochschild chain complex:
  \[
  \hh_n(A,M)\defined \hlgy^n\hohcomp(A,M)
  \]
  In the case $M=A$, we simply write $\hh^n(A)$ and $\hh_n(A)$ for Hochschild cohomology respectivley homology.
  \end{enumerate}
\end{construction}
\subsection{Interpretations of Hochschild (co)homology}
\begin{numtext}
  With the free resolution $\cbarb(A)$ at hand, we can describe Hochschild (co)homology using the language of derived functors: The free resolution is in particular a projective resolution of $A$ as an $A$-bimodule. Consequently, it serves to calculate the right derived functors of the left-exact functor
  \[
  \hom_{A^e}(A,-)\mc A^e\modcat\to\kvec,
  \]
  i.e.
  \[
    \ext^i_{A^e}(A,M)\defined \rder^i(\hom_{A^e}(A,-))(M) = \hlgy^i(\hom_{A^e}(A,M))),
  \]
  which is precisley our defintion of Hochschild cohomology. Similarly, Hochschild homology can be interpreted as the left-derived functors of the right exact functor
  \[
  A\tensor_{A^e}-\mc A^e\modcat\to \kvec,
  \]
  since the free resolution also provides a flat resolution. However, we sometimes need this explicit resolution:
  \begin{itemize}
  \item In order to interpret Hochschild (co)homology, it turns out that understanding the differential of the bar resolution is crucial. But this is not part of the datum of the derived functors.
  \item We also need it to find the additional structure on Hochschild (co)homology we are interested in.
  Apparently, it is a question of ongoing research how different choices of resolutions lead to different structures$.^{\text{\small{(\textit{citation needed})}}}$
  \end{itemize}
\end{numtext}
\begin{rem}
  Extend this to a general derived defintion.
\end{rem}
\begin{prop}\label{defintions:k-isomorphic-construction}
\leavevmode
\begin{enumerate}
\item
  The following map is an isomorphism of $k$-vector spaces:
  \begin{align*}
    \hohcompg^n(A,M)
    &
    \xrightarrow{\sim}
    \hom_k(A^{\tensor n},M)\\
    \left[g\mc A^{\tensor n+2}\to M\right]&\mapsto \left[a_1\tensor\ldots\tensor a_n\mapsto g(1\tensor a_1\tensor \ldots \tensor a_n\tensor 1)\right]
  \intertext{where the inverse is given by:}
  \hom_k(A^{\tensor n},M)
  &
  \xrightarrow{\sim}
  \hohcompg^n(A,M)
  \\
  f
  &
  \mapsto
  \left[a_0\tensor \ldots \tensor a\np \mapsto a_0f(a_1\tensor\ldots\tensor a_n)a\np\right]
  \end{align*}
  Under this isomorphism, precomposing with the differential gives a map
  \begin{align*}
  \hom_k(A^{\tensor n},M)&
  \to
  \hom_k(A^{\tensor n+1},M)
  \\
  \left[A^{\tensor n}\xrightarrow{f}M\right]
  &
  \mapsto
  \left[A^{\tensor n+1}\xrightarrow{d_{n-1}}A^{\tensor n}\xrightarrow{f}M\right],
  \end{align*}
  where for such a $f\mc A^{\tensor n}\to M$, the linear map $df$ explicitly looks like
  \begin{equation}\label{definition:hochschild-differential}
  \begin{split}
    df(a_1\tensor\ldots\tensor a\np)
    &=
    a_1 f(a_2\tensor \ldots \tensor  a_{n+1})
    \\
    &+
    \sum_{i=1}^n\left(-1\right)^i f(a_1\tensor\ldots\tensor a_i a_{i+1}\tensor\ldots \tensor a\np)
    \\
    &+
    \left(-1\right)^{n+1}f(a_1\tensor\ldots\tensor a_n)a\np.
  \end{split}
  \end{equation}
  We call this induced map the \emph{Hochschild differential}.
\item Similarly, for Hochschild homology, we have a $k$-linear isomorphism
\begin{align*}
  \hohcompg_n(A,M)
  &
  \xrightarrow{\sim}
  M\tensor_k A^{\tensor n}
  \\
  m\tensor_{A^e}a_0\tensor\ldots\tensor a\np
  &
  \mapsto
  \left(a\np m a_0\right)
  \tensor
  a_0\tensor \ldots \tensor a_n
  \intertext{with the inverse map given by:}
  M\tensor_k A^{\tensor n}
  &
  \xrightarrow{\sim}
  \hohcompg_n(A,M)
  \\
  m\tensor a_1\tensor \ldots \tensor a_n
  &
  \mapsto
  m\tensor_{A^e}\tensor 1
  \tensor a_1\tensor\ldots\tensor a_1\tensor 1
\end{align*}
  Again, precomposing with the differential gives maps
  \[
  d\mc M\tensor_k A^{\tensor n}\to M\tensor_k A^{\tensor n-1}
  \]
  which are explicitly given by
  \begin{align*}
    d(m\tensor a_1\tensor \ldots \tensor a_n)
    &=
    (ma_1)\tensor a_2\tensor \ldots \tensor a_n \\
    &+ \sum_{i=1}^n\left(-1\right)m\tensor a_1\tensor \ldots \tensor a_ia_{i+1}\tensor\ldots a_n \\
    &+ \left(-1\right)^n a_nm\tensor a_1\tensor \ldots \tensor a_{n-1}.
  \end{align*}
\end{enumerate}
In particular, these complexes lead to isomorphic groups of Hochschild (co)homology.
\end{prop}
%TODO: Add stuff about the group cohomology already

\subsection{Hochschild (co)homology in low degrees}
\subsubsection*{Hochschild homology in degree zero}
\begin{numtext}
  The ``end'' of the Hochschild chain complex is given by
  \begin{align*}
    \ldots
    \to
    M\tensor_k A
    &
    \xrightarrow{d}
    M
    \to 0
    \\
    m\tensor a
    &
    \mapsto
    ma-am;
  \end{align*}
  so we immediatley get $\hh_0(A,M) \cong M/[M,A]$, which is also called the \emph{module of coinvariants}. In particular, if $M=A$, then $\hh_0(A)$ is the abelianization of $A$, and if $A$ is commutative then $\hh_0(A) = A$. If $A$ is however not commutative, then there is in general no good additional structure on $A/[A,A]$.
\end{numtext}

\subsubsection*{Hochschild cohomology in degrees zero, one and two}
\begin{numtext}
Using again the identification from \cref{defintions:k-isomorphic-construction}, we write the first terms of the Hoschild cochain complex as:
\[
0
\to
M
\xrightarrow{d}
\hom_k(A,M)
\xrightarrow{d}
\hom_k(A^{\tensor 2},M)
\xrightarrow{d}
\hom_k(A^{\tensor 3},M)
\xrightarrow{d}
\ldots
\]
where:
\begin{itemize}
  \item
  For an element $m\in M$ the map $d(m)\mc A\to M$ is given by
  \[
  a\mapsto am - ma.
  \]
  \item
  For a map $f\mc A\to M$, the map $d(f)\mc A^{\tensor 2}\to M$ is given by
  \[
  a\tensor b \mapsto af(b)-f(ab)+f(a)b.
  \]
  \item
  For a map $g\mc A^{\tensor 2}\to M$, the map $d(g)\mc A^{\tensor 3}\to M$ is given by
  \begin{align*}
    a\tensor b \tensor c & \mapsto a g(b\tensor c)- g(ab\tensor c)\\
                         & + g(a\tensor bc) - g(a\tensor b)c.
  \end{align*}
\end{itemize}
From this we already see that
\[
\hh^0(A;M) = \lset m\in M\ssp am = ma\text{ for all }a \in A\rset,
\]
which is also called the \emph{module of invariants of $M$}. In particular, if $M=A$, then $\hh^0(A) = \centre(A)$ is its center.
Since taking the center is in general not functorial of $k$-algebras, we see that we can't get a functor $\coh(-)\mc k\text{-}\algcat\to k\text{-}\algcat$.
\end{numtext}
Before we can analyse the first and second Hochschild cohomology, we need some more terminology:
\begin{construction}
  \leavevmode
  \item A $k$-linear map $f\mc A\to M$ is called a \emph{derivation} (or more precisley: an $A$-derivation) if
  \[
  f(ab) = af(b) + f(a)b
  \]
  holds for all $a,b\in A$. We denote the set of all derivations on $M$ by $\der_k(A,M)$; this is a $k$-linear subspace of $\hom_k(A,M)$.
  \glsadd{derivations}
  \item For an element $m\in M$, the associated \emph{inner derivation} is the map
  \[
  \ad_m\mc A\to M,~a\mapsto am - ma.
  \]
  We call a derivation \emph{inner} if it's of the form $\ad_m$ for an $m\in M$. The subspace of all inner derivations is denoted by $\innder_k(A,M)$. We define the vector space of \emph{outer derivations} to be the quotient
  \[
  \outder_k(A,M)\defined \der_k(A,M)/\innder_k(A,M).
  \]
\end{construction}
\begin{rem}
  Note that if $A$ is commutative then there are no non-trivial inner derivations on an $A$-module $M$.
\end{rem}
\begin{prop}\label{defintions:first-hochschild-co-derivations}
  Let $M$ a module over the associative $k$-algebra $A$. Then the first Hochschild cohomology of $M$ is given by the outer derivations on $M$:
  \[
  \hh^1(A;M)\cong \outder_k(A,M).
  \]
\end{prop}
\begin{proof}
  We first note that the kernel of the first Hochschild differential
  \[
  d^1\mc \hom_k(A,M)\to \hom_k(A\tensor A,M)\]
   is given by
  \[
  \lset f\mc A\to M\ssp af(b) - f(ab) + f(a)b = 0\text{ holds for all } a,b\in A\rset
  \]
  which is precisley the definition of $\der_k(A,M)$. We also have that the image of the zeroth Hochschild differential consists of all $f\mc A\to M$ such that there is an $m\in M$ with $d(m) = f$, i.e.
  \[
  f(a) = d(m)(a) = am - ma,
  \]
  which are precisley the inner derivations.
  So we indeed have
  \[\hh^1(A;M) = \der_k(A,M)/\innder_k(A,M) = \outder_k(A,M).\]
\end{proof}
We now want to consider again the special case of $M=A$ and look for more structure on $\hh^1(A)$.
\begin{defn}
  Let $D_1,D_2\mc A\to A$ be two derivations. Then their \emph{bracket} to be the map
  \[
  D_1\circ D_2 - D_2 \circ D_1 \mc A\to A
  \]
  \glsadd{bracket-of-derivations}
\end{defn}
\begin{lem}
  \leavevmode
  \begin{enumerate}
    \item The bracket $[D_1,D_2]$ of two derivations $D_1,D_2\mc A\to A$ is again a derivation.
    \item For an element $a\in A$ and a derivation $D\mc A\to A$ it holds that \[[D,\ad_a] = \ad_{D(a)}.\]
  \end{enumerate}
\end{lem}
\begin{proof}
  Both are straight-forward calculations:  For i), we have for $a,b\in A$:
  \begin{align*}
    [D_1,D_2](ab) &= D_1(D_2(ab)) - D_2(D_1(ab))
    \\
    &= D_1\left(aD_2(b) + D_2(a)b\right)\\
    &- D_2\left(aD_1(b) + D_1(a)b\right)\\
    &= aD_1D_2(b) + D_1(a)D_2(b)
    \\
    &- D_2(a)D_1(b) - D_1D_2(a)b
    \\
    &- aD_2D_1(b) + D_2(a)D_1(b)\\
    &-D_1(a)D_2(b) + D_2D_1(a)b\\
    &= a[D_1,D_2](b) + [D_1,D_2](a)b
  \end{align*}
  For ii) we have
   \begin{align*}
     [D,\ad_a](b) &= D\left(\ad_a(b)\right)\\
     &= D(ab-ba) - aD(b) + D(b)a
     \\
     &= aD(b) + D(a)b
     - D(b)a-bD(a)
     - aD(b)+D(b)a
     \\
     &= D(a)b - bD(a)
     \\
     &= \ad_{D(a)}(b).
   \end{align*}
\end{proof}
\begin{cor}
  \leavevmode
  \begin{enumerate}
    \item The bracket of derivations turn $\der(A)$ into a Lie algebra.
    \item The $k$-subspace $\innder(A)$ is an ideal of this Lie algebra and thus $\outder(A)$ has a natural Lie algebra structure.
  \end{enumerate}
\end{cor}
\begin{proof}
  \leavevmode
  \begin{enumerate}
    \item By i) of the above calculation, $\der(A)$ becomes a subalgebra of $\liegl(A)$.
    \item That $\innder(A)$ is a Lie ideal in $\der(A)$ means that for all $D\in \der(A)$ and $\ad_a\in\innder(A)$ it holds that $[D,\ad_a]\in \innder(A)$; this follows immediatley from ii) in the calculation above.
  \end{enumerate}
\end{proof}
We need even more structure to understand the second Hochschild cohomology:
\begin{construction}
\leavevmode
\begin{enumerate}
\item
  Let $M$ be an $A$-bimodule and $f\mc A\tensor_k A\to M$ a $k$-linear map that satisfies the following cocylce condition in $M$:
  \begin{equation}\label{definitions:square-zero-cocycle}
    a_0 g(a_1\tensor a_2)
    -
    g((a_0a_1)\tensor a_2)
    +
    g(a_0\tensor (a_1a_2))
    -
    g(a_0\tensor a_1)a_2
    =
    0
  \end{equation}
  Such a map is called a \emph{factor set}.
  We use this to define the structure of a $k$-algebra on the direct sum $A\oplus M$ by
  \[
  \begin{tikzcd}[ampersand replacement=\&]
    (A\oplus M)
    \ktensor
    (A\oplus M)
    \ar{d}[right]{\cong}
    \\
    (A\tensor A)
    \oplus
    (A\tensor M)
    \oplus
    (M\tensor A)
    \oplus
    (M\tensor M)
    \ar{dd}[right]{\begin{pmatrix}
      \mu & 0 & 0 &0\\
      g & \mu_l & \mu_r & 0
    \end{pmatrix}}
    \\
    \\
    A\oplus M
  \end{tikzcd},
  \]
  where $\mu\mc A\tensor A\to A$ is the multiplication on $A$ and $\mu_l,\mu_r$ are the left respective right multiplication of $A$ on $M$.
  Spelled out, this means
  \[
  (a_1,m_1)\cdot (a_2,m_2)
  =
  (a_1a_2, a_1 m_2 + m_1 a_2 + g(a_1\tensor a_2)).
  \]
  That $g$ satisfies the cocylce condition is necessary for the algebra structure to be associative.
  We call $A\oplus M$ together with the algebra structure induced by $g$ the \emph{semi-direct product induced by $g$} and denote it by $A\semip^g M$.
  In the case that $g$ is the zero map, we only write $A\semip M$.
  \item
  A particular instance of this comes from \emph{square-zero} extensions:
  Consider a surjective map of $k$-algebras $f\mc E\twoheadrightarrow A$, such that $\left(\ker(f)\right)^2 = 0$.
  Then $\ker(f)$ becomes an $A$-bimodule in the following way:
  For $a\in A$, choose a lift $e\in f^{-1}(a)$ and set for an $m\in \ker f$:
   \begin{equation}\label{definitions:square-zero-multiplication}
   a\cdot m \defined em\text{ and }m\cdot a \defined me.
   \end{equation}
   This is well-defined, since for two different lifts $e,e'$, the difference $e-e'$ is an element in $\ker(f)$ and hence
   \[
   (e-e')m,~m(e-e')\in \left(\ker(f)\right)^2 = 0.
   \]
   Furthermore, every choice of a $k$-linear section $s\mc A\to E$ provides us with a factor set:
   \[
   g\mc A\tensor_k A
   \to
   \ker(f)
   ,
   ~
   a_1\tensor a_2
   \mapsto
   s(a_1a_2) - s(a_1)s(a_2)
   \]
   The cocylce condition is satisfied since the definition of the multiplication in \eqref{definitions:square-zero-multiplication} turns $\ker(f)\sse E$ into an associative subalgebra.
   We call an $A$-bimodule $M$ together with a surjective map $f\mc E\twoheadrightarrow A$ that satisfies $(\ker(f))^2 = 0$ and $\ker f\cong M$ as $A$-bimodules a \emph{square-zero extension of $A$ by $M$} (via $f$).
   Note that the algebra structure (e.g. $A\semip^g M$ for the factor set from above) is not part of the data of a square-zero extension.
   But every factor set leads to a square-zero extension:
   \begin{claim}\label{definitions:square-zero-by-factor}
     Let $M$ be an $A$-bimodule and $g\mc A\tensor A\to M$ a factor set.
     Then the canonical projection $\pi \mc A\semip^g M\twoheadrightarrow A$ is a square-zero extension.
   \end{claim}
   \begin{claimproof}
     We need to check two things: First of all that the projection $A\oplus M\twoheadrightarrow A$ is compatible with the algebra structure induced by $g$. But this is immediate, since $A\hookrightarrow A\semip^g M$ is an algebra homomorphism and they compose to the identiy (aka. the multiplication restricted to the first component is just the multiplication in $A$).
     Secondly, we need to see that the kernel of $\pi$ (i.e. $M$) squares to zero (which is immediate from the definition of the multiplication) and that the bialgebra structure from \cref{definitions:square-zero-multiplication} agrees with the bialgebra structure on $M$ we started with (but we can just choose the canonical lift).
     So we're done.
   \end{claimproof}
   \item
   Given two square-zero extensions of $A$ by the same $A$-bimodule $M$, induced by $f\mc E\twoheadrightarrow A$ and $f'\mc E'\twoheadrightarrow A$,
   we say that they are \emph{equivalent} if there exists an algebra isomophism $\pphi\mc E\to E'$ such that the diagram
   \[
   \begin{tikzcd}
   0
   \ar{r}
   &
   M
   \ar[equal]{d}
   \ar{r}
   &
   E
   \ar{d}[right]{\pphi}[left]{\cong}
   \ar{r}[above]{f}
   &
   A
   \ar[equal]{d}
   \ar{r}
   &
   0
   \\
   0
   \ar{r}
   &
   M
   \ar{r}
   &
   E'
   \ar{r}[below]{f'}
   &
   A
   \ar{r}
   &
   0
   \end{tikzcd}
   \]
   commutes. We denote the $k$-vector space of all square-zero extensions of $A$ by $M$ by $\algext_k(A,M)$
   (Note that the equivalence relation is again compatible with the addition and scalar multiplication of $k$-algebra maps.).
   In particular we have that different choices of isomophism for a fixed $A$-bimodule $M$ and a surjection $E\twoheadrightarrow A$, say \[\psi_1,\psi_2\mc \ker f\isomorphism M\] lead to equivalent extensions, by considering the diagram
   \[
   \begin{tikzcd}[ampersand replacement=\&, column sep = huge]
   0
   \ar{r}
   \&
   \ker f
   \ar[equal]{d}
   \ar{r}[above]{(\psi_1,0)}
   \&
   M\oplus A
   \ar{d}[right]{\begin{pmatrix}\psi_2\circ \psi_1^{-1}&0\\0&\id_A\end{pmatrix}}
   \ar{r}[above]{f}
   \&
   A
   \ar[equal]{d}
   \ar{r}
   \&
   0
   \\
   0
   \ar{r}
   \&
   \ker f
   \ar{r}[above]{(\psi_2,0)}
   \&
   M\oplus A
   \ar{r}[below]{f'}
   \&
   A
   \ar{r}
   \&
   0
   \end{tikzcd}
   \]
\end{enumerate}
\end{construction}
\begin{prop}
  The association
  \[
  \zker(\hohcompg^2(A,M))
  \to
  \algext_k(A,M),
  \left[g\mc A^{\tensor 2}\to M\right]
  \mapsto
  A\semip^g M
  \]
  is well-defined and descents to an isomorphism of $k$-vector spaces
  \[
  \hh^2(A,M)
  \isomorphism
  \algext_k(A,M).
  \]
\end{prop}
\begin{proof}
  Loads of things to check, but none of them too difficult:
  \begin{enumerate}
    \item The map is well-defined:
    First of all, we need to check that every Hochschild cocylce $g\in \zker(\hohcompg^2(A,M))$ satisfies the cocycle condition \eqref{definitions:square-zero-cocycle}.
    This is a direct consequence of $g$ being in the kernel of the second Hochschild differential --- by \eqref{definition:hochschild-differential} we have for all $a_0,a_1,a_2\in A$ the identity:
    \begin{align*}
    0
    &=
    df(a_0\tensor a_1 \tensor a_2)
    \\
    &=
    a_0 f(a_1\tensor a_2)
    -
    f((a_0a_1)\tensor a_2)
    +
    f(a_0\tensor (a_1 a_2))
    -
    f(a_0\tensor a_1)a_2,
    \end{align*}
    which is precisley the cocycle condition.
    In particular, factor sets are the same as Hochschild $2$-cocycles.
    By \cref{definitions:square-zero-by-factor}, we also know that $A\semip^g M$ is indeed a square-zero extension, so this map is indeed well-defined.
    \item
    Every square-zero extension of $A$ by $M$ gives rise to a factor set:
    Let $f\mc E\twoheadrightarrow A$ be a surjective algebra map that induces the square-zero extension.
    Since we are working with $k$-vector space, we can choose a noncanonical splitting $s\mc A\to E$, which induces an isomophism of vector spaces $E\cong A\oplus M$.
    Under this isomorphism, the multiplication on $E$ can be written as
    \begin{equation}\label{definitions:decomposition-of-mult}
    (a,m)\cdot (b,n) =
    (ab, an+mb+s(ab)-s(a)s(b)),
    \end{equation}
    and associativity of the multiplication on $E$ implies that
    \[
    g\mc A\tensor A \to M,
    ~
    a\tensor b
    \mapsto
    s(ab) - s(a)s(b)
    \]
    is the desired factor set.
    \item
    Two factor sets $g,g'$ induce the same algebra structure on $A\oplus M$ if and only if $g-g'$ is a Hochschild boundrary:
    Let $s'\mc A\to E$ be a different splitting.
    Then we have
    \begin{align*}
      g'(a,b) - g(a,b)
      &=
      \left(s'(a)s'(b)-s'(ab)\right) -
      \left(s(a)s(b)- s(ab)\right)
      \\
      &=
      s'(a)\left(s'(b)-s(b)\right)
      \\
      &- \left(s'(ab)-s(ab)\right)
      \\
      &+ \left(s'(a)-s(a)\right)s(b)\\
      &=
      d(s-s')(a\tensor b),
    \end{align*}
    and hence $g'-g$ agree up to boundrary.
    The description of the multiplication on $A\oplus M$ in \cref{definitions:decomposition-of-mult} yields that they give the same algebra structure on $A\oplus M$.
  \end{enumerate}
\end{proof}
\begin{rem}
  We could have also deduced this isomorphism from the $\ext$-description of Hochschild cohomology, or at least I think we could.
\end{rem}

\subsection{First examples}
Calculating Hochschild (co)homology of an algebra $A$ is ``just'' a matter of finding a ``good'' resolution of $A$ as a left $A^e$-module.
If $A$ is commutative, this is the same as a resolution of $A$ as a  $A\tensor_k A$-module.
The resolutions we defined so far all have arbitrary high degrees, so we need to hand-craft better ones for this ad-hoc approach.
Later, we can use the HKR decomposition to simplify these calculations.
\begin{example}\label{definitions:basic-examples}
  \leavevmode
  \begin{enumerate}
    \item \label{definitions:basic-examples:poly}
    The polynomial ring $k[t]$: We have that the enveloping algebra of $k[t]$ is $k[t]\tensor k[t] \cong k[x,y]$, and a free resolution of $k[t]$ as a $k[x,y]$-module is given by:
    \[
    \begin{tikzcd}[cramped, row sep = small]
    0
    \ar{r}
    &
    k[x,y]
    \ar{r}[above]{\cdot(x-y)}
    &
    k[x,y]
    \ar{r}
    &
    k[t]
    \ar{r}
    &
    0,
    \end{tikzcd}
    \]
    where the last map sends both $x$ and $y$ to $t$.
    That this resolution has lenght $2$ implies immediatley that
    \[
    \hh_i(k[t]) = \hh^i(k[t]) = 0\text{ for }i\geq 3.
    \]
    For Hochschild homology, we have
    \begin{align*}
      \hh_0(k[t])
      &
      =
      \left(k[x,y]\tensor k[t]\right)
      /
      \left(\im\left(k[x,y]\tensor k[t]\xrightarrow{\cdot(x-y)\tensor \id}\right)\right)
      \\
      &
      \cong
      \left(k[x,y]\tensor k[t]\right)
      /
      \left(k[t]\tensor k[t]\right)
      \\
      &
      \cong
      k[t]
      \\
      \hh_1(k[t])
      &
      =
      \ker\left(k[x,y]\tensor k[t]\xrightarrow{\cdot(x-y)\tensor \id}k[x,y]\tensor k[t]\right)
      \\
      &
      \cong
      k[t],
      \intertext{and similarly, for Hochschild cohomology}
      \hh^i(k[t])
      &
      \cong
      \begin{cases}
        k[t]&\text{if }i=0,1;\\
        0&\text{otherwise}.
      \end{cases}
    \end{align*}
  \item For finite dimensional algebras $A$ over $k$ of finite global dimensions, we can use path algebras to deduce
  \[
  \hh_i(A)
  =
  \begin{cases}
    k^r&\text{if }i=0;\\
    0&\text{otherwise},
  \end{cases}
  \]
  where $r$ is the number of isomorphism classes of simple $A$-modules.
  Note that this is different from the case of infinite-dimensional algebras over $k$ that have finite global dimension (like the polynomial ring).
  However, we at least have that all algebras of finite global dimension have bounded Hochschild (co)homology.
  \item \label{definitions:basic-examples:poly:non-flat}
  When the rings in question don't have finite global dimension, even more can happen.
  Looking ahead, we want to calculate the Hochschild (co)homology of $A = k[t]/t^n$ for $n\geq 1$.
  If we set $u\defined t\tensor 1 - 1 \tensor t\in A^e$ and
  \[
  v\defined \sum_{i=0}^{n-1}
  t^{n-1-i}\tensor t^i,
  \]
  then a free resolution of $A$ as an $A^e$-module is given by
  \[
  \ldots
  \xrightarrow{\cdot v}
  A^e
  \xrightarrow{\cdot u}
  A^e
  \xrightarrow{\cdot v}
  A^e
  \xrightarrow{\cdot u}
  A^e
  \xrightarrow{x\tensor y \mapsto xy}
  A
  \to
  0.
  \]
  Applying $\hom_{A^e}(-,M)$ to the resolution part gives
  \[
  \begin{tikzcd}[row sep = small, cramped]
  0
  \ar{r}
  &
  M
  \ar{r}[above]{0}
  &
  M
  \ar{r}[above]{\cdot nt^{n-1}}
  &
  M
  \ar{r}[above]{0}
  &
  M
  \ar{r}[above]{nt^{n-1}}
  &
  M
  \ar{r}[above]{0}
  &
  \ldots
  \end{tikzcd}
  \]
  Since we're working in characteristic zero, we get
  \[
  \hh^i(A,M)
  \begin{cases}
    M&\text{if }i=0;
    \\
    M/(t^{n-1}M)
    &
    \text{otherwise.}
  \end{cases}
  \]
  The similar result holds for Hochschild homology.
  So in particular for $A=M$, we have
  \[
  \hh_i(A)
  =
  \begin{cases}
    k[t]/t^n&\text{if }i=0;
    \\
    k[t]/(t^{n-1})
    &
    \text{otherwise.}
  \end{cases}
  \]
  This shows in particular that $\hh_i(A)$ is not necessarily flat as an $A$-module.
  (That a quotient $R/I$ is a flat $R$-module implies $I^2 = I$, which does not hold here.)
  \end{enumerate}
\end{example}

\subsubsection*{For finite-dimensional algebras using quivers}
\begin{numtext}
  We now want to spell out a general approach to calculating the Hochschild cohomology of a finite-dimensional $k$-algebra that is not necessarily commutative, using methods from representation theory.
  In general, the $\ext$-description
  \[
  \hh^n(A)
  =
  \ext^n_{A^e}(A,A)
  \]
  tells us that ``all'' we need to do is to find a ``good'' resolution of $A$ as an $A^e$-module.
  If $A$ is a finite-dimensional $k$-algebra, then there is a finite quiver $Q$ and an admissible ideal $I\sse kQ$ such that
  \[
  A\modcat \cong (kQ/I)\modcat
  \]
  holds (this is called \emph{Morita equivalence}).
  In this case, there are (up to isomorphism) finitely many indecompsable projective objects in the category $A\modcat$, which can be described in terms of the vertices of the quiver (\coms todo: add details\come)
  We then have that the set of
  \[
  P(i,j)
  \defined
  Ae_i\tensor e_j A
  \]
  for $1\leq i,j\leq \dim(A)$ are the indecompsable projectives of $A^e\modcat$.
  We have the following description of a minimal projective resolution of $A$ as an $A^e$-module, which is due to Happel:
\end{numtext}
\begin{theorem}
  Denote by $S(i)$ the simple $A$-modules and set
  \begin{align*}
    e_{ij}^m
    &
    \defined
    \dim_k \ext_A^m(S(i),S(j))
    \intertext{as well as}
    P_m
    &
    \defined
    \bigoplus_{i,j = 1}^n
    P(i,j)^{\oplus e_{ij}^m}.
  \end{align*}
  Then there is a minimal projective resolution of $A$ as an $A^e$-bimodule of the form
  \[
  \begin{tikzcd}[column sep = small, cramped]
    \ldots
    \ar{r}
    &
    P_2
    \ar{r}
    &
    P_1
    \ar{r}
    &
    P_0
    \ar{r}
    &
    A
    \ar{r}
    &
    0
  \end{tikzcd}.
  \]
\end{theorem}
\begin{rem}
  \leavevmode
  \begin{enumerate}
    \item
    We can at least calculate some of the $e_{ij}^m$:
    If $m = 0$, then Schur's lemma (the $S(i)$ are simple) implies
    \begin{equation}\label{defn:quivers:zero-case}
    e_{ij}^0
    =
    \begin{cases}
      1
      &
      \text{if $i=j$;}
      \\
      0
      &
      \text{otherwise.}
    \end{cases}
    \end{equation}
    Using the description of $\ext^1$ as group of Yoneda-extensions with the Baer-sum, we have
    \[
    e_{ij}^1
    =
    \#
    \lset
    \alpha \in Q_1
    \ssp
    s(\alpha) = e_i, t(\alpha)=e_j
    \rset.
    \]
    \item We also get a bound on the Hochschild cohomology of $A$; namely if $A$ has finite global dimension $d$, then $\hh^m(A) = 0$ for all $m>d$.
  \end{enumerate}
\end{rem}
\begin{cor}
  Let $A=kQ/I$ be the quotient of the path algebra of an acyclic quiver $Q$ by an admissible ideal $I$.
  Then:
  \[
  \dim_k
  \hh_m(A)
  =
  \begin{cases}
    \# Q_0
    &
    \text{if $m=0$;}
    \\
    0
    &
    \text{otherwise.}
  \end{cases}
  \]
\end{cor}
\begin{proof}
  Denote by $P_i$ the modules in Happel's minimal projective resolution.
  We want to show that
  \[
  P_m \tensor_{A^e} A
  =
  \begin{cases}
    k^{\oplus n}
    &
    \text{if $m = 0$;}
    \\
    0
    &
    \text{otherwise}
  \end{cases}
  \]
  where we set $n\defined \dim_k A$.
  In the case $m =0$, we have
  \begin{align*}
    P_0\tensor_{A^e}A
    &=
    \bigoplus_{i=1}^n Ae_i\tensor e_i A
    &
    \text{(by \ref{defn:quivers:zero-case})}
    \\
    &
    \cong
    \bigoplus_{i=1}^n e_iAe_i
    &
    \\
    &
    \cong
    k^{\oplus n}.
    &
    \text{($e_iAe_i = k$)}
  \end{align*}
  For the case $m\geq 1$, we need more calculations, which basically boil down to using that the quiver is acyclic.
  This relies on the following fact:
  \begin{fact}
    Consider a minimal projective resolution of the simple module $S(i)$ as an $A$-module.
    Then each term $T_s$ in this resolution is of the form
    \[
      T_s \cong
      \bigoplus_{v\in V_s}(Av)^{\oplus \alpha_{s,v}}
    \]
    for some $\alpha_{s,v}\in \nn$, where
    \[
    V_s
    \defined
    \lset v\in Q_0 \ssp
    \text{there is path from $v$ to $e_i$ of lenght $s$}
    \rset.
    \]
  \end{fact}
  Now this implies
  \[
  \ext^s_A(S(i),S(j))
  =
  0,
  \]
  unless there is a path from $j$ to $i$ of lenght $s$, and in particular,
  \[
  \ext^s_A(S(i),S(i)) = 0
  \]
  for $s\geq 1$ (since there are no loops).

\end{proof}

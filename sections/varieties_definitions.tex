\section{Hochschild (Co)homology for varieties -- Definitions}
\subsection{Fourier-Mukai transformations}
\begin{construction}\label{varieties:basic-derived-functors}
  \leavevmode
  \begin{enumerate}
    \item Let $f\mc X\to Y$ be a morphism of schemes.
    Then the \emph{inverse image functor} $\inverseimage{f}$ and the \emph{direct image functor} $\directimage{f}$ from the adjunction
    \[
    \inverseimage{f}\mc\qcohcat(Y)\leftrightarrows\qcohcat(X)\mcc \directimage{f}
    \]
    are right and left adjoint respectively, and hence give an adjunction
    \[
    \lderived \inverseimage{f}\mc \derivedd(\qcohcat(Y))\leftrightarrows\derivedd(\qcohcat(X))\mcc \rderived \directimage{f}.
    \]
    \item Similarly, for any quasi-coherent module $\she$, we have the adjunction
    \[
    \she\tensor_{\ox}-\mc \qcohcat(X)\leftrightarrows\qcohcat(X)\mcc\shhom(\she,-)
    \]
    which gives rise to adjoint functors
    \[
    \she\dtensor-\mc \derivedd(\qcohcat(X))\leftrightarrows \derivedd(\qcohcat(X))\mcc \rderived\shhom(\she,-).
    \]
    \item The functor $\rderived \directimage{f}$ also has a right-adjoint, which is commonly denoted by $\shriek{f}$.
    To define it we need to enter the realm of Grothendieck-Verdier duality (see \cite{hubible}*{3.4}).
    But I at least want to try to provide some examples of this: Let $f\mc X\to Y$ be a morphism of smooth schemes over a field $k$ of relative dimension $\dim(f)\defined \dim(X)-\dim(Y)$.
    Denote by $\omega_X$ and $\omega_Y$ the \emph{canonical sheaves} on $X$ and $Y$ respectively (i.e. $\omega_X \defined \exterior^{\dim(X)}\kahl{X}{k}$)
    Then the \emph{relative dualizing bundle} of $f$ is defined as
    \[
    \omega_f\defined
    \omega_X\tensor \inverseimage{f}\omega_Y^{-1}.
    \]
    \glsadd{relative-dualizing-bundle}
    Then the functor
    \[
    \shriek{f}\mc \derivedd(\qcohcat(Y))\to \derivedd(\qcohcat(X))
    \]
    is defined as
    \[
    \she\mapsto \lderived\inverseimage{f}(\she)\tensor \omega_f[\dim(f)].
    \]
  \end{enumerate}
\end{construction}
I feel like this is the appropriate place to at least state Grothendieck-Verdier Duality:
\begin{theorem}[Grothendieck-Verdier Duality]\label{varities_defn:gvduality}
  Let $f\mc X\to Y$ be a morphism of smooth schemes over a field $k$ of relative dimension $\dim(f)$, and denote by $\omega_f$ its relative dualizing bundle (as above).
  Then for any $\shf\in \bderivedd(X)$ and $\she\in \bderivedd(Y)$ there exists a functorial isomorphism
  \[
    \rderived \directimage{f}\rderived\shhom(\shf,\shriek{f}\she)
    \cong
    \rderived \shhom(\rderived\directimage{f}\shf,\she).
    \]
\end{theorem}
\begin{lem}\label{varieties:projection-formula}
  Let $\she\in \derivedd(\qcohcat(X))$ and $\shf\in \derivedd(\qcohcat(Y))$.
  Then there is an isomorphism
  \[
  \rderived \directimage{f}(\she)\dtensor \shf
  \cong
  \rderived \directimage{f}\left(E\dtensor \lderived \inverseimage{f}\shf\right)
  \]
  This is called the \emph{projection formula}.
\end{lem}
\begin{rem}
  In the case that $X$ and $Y$ are smooth and projective schemes over $k$, all the functors from \cref{varieties:basic-derived-functors} restrict to functors between the bounded derived categories of coherent modules, which we denote by
  \[
  \bderivedd(X)\defined \bderivedd(\cohcat(X))\text{ and }
  \bderivedd(Y)\defined \bderivedd(\cohcat(Y))
  \]
\end{rem}
\begin{nonumconvention}
  From now on, we will assume that $X$ and $Y$ are smooth and projective over $k$, if not otherwise specified.
\end{nonumconvention}
\begin{defn}
  Let $\she\in \bderivedd(X\times Y)$, where $X$ and $Y$ are smooth projective varities over $k$.
  Then the \emph{Fourier-Mukai transformation} with \emph{kernel} $\she$ is given by the functor
  \[
  \Phi_{\she}\mc \bderivedd(X)\to\bderivedd(Y),~
  \shf \mapsto
  \rderived \directimage{q}\left(\inverseimage{p}\shf \dtensor \she\right),
  \]
  where $p$ and $q$ are the projections from $X\times Y$ onto $X$ and $Y$ respectively
\end{defn}
\glsadd{fourier-mukai}
\begin{example}
\leavevmode
\begin{enumerate}
  \item
  The identity functor
  \[
  \id \mc \bderivedd(X)\to \bderivedd(X)
  \]
  can be regarded as the Fourier-Mukai transformation with kernel $\directdelta\ox$, where
  \[
  \Delta\mc X\hookrightarrow X\times X
  \]
  denotes the diagonal embedding:
  We have that $\Delta$ is a closed immersion, so $\rderived \directimage{\Delta} = \directimage{\Delta}$ holds (as the direct image of a closed immersion is always exact).
  We now have
  \begin{align*}
    \Phi_{\directimage{\Delta}\ox}
    &=
    \rderived\directimage{q}\left(\inverseimage{p}\shf\dtensor \directimage{\Delta}\ox\right)
    \\
    &\cong
    \rderived \directimage{q}\left(\directimage{\Delta}\left(\lderived \inverseimage{\Delta}\circ \inverseimage{p}\shf \dtensor \ox\right)\right)\\
    &\cong
    \directimage{\rderived(q\circ \Delta)}
    \circ
    \directimage{\lderived(\Delta\circ p)}
    \\
    &\cong \shf,
  \end{align*}
  where we used the projection formula from \cref{varieties:projection-formula} and the fact that deriving is compatible with compositions.
  \item
  Let $f\mc X\to Y$ be a morphism and denote by $\Gamma_F\sse X\times Y$ its graph.
  Then similarly to the above we get that
  \[
  \directimage{f}\cong \Phi_{\oo_{\Gamma_f}}\mc \bderivedd(X)\to \bderivedd(Y),
  \]
  since the map $\iota\mc X\to \oo_{\Gamma_f}$ is a closed immersion and again $\oo_{\Gamma_f}\cong \directimage{\iota}\oo_X$ hollds.
  In particular, we recover the above result since $\Delta = \Gamma_{\id}$.
  We also get that $\inverseimage{f}$ can be regarded as the Fourier-Mukai transformation in the opposite direction.
  \item Let $L$ be a line bundle on $X$, then the functor
  \[
  \bderivedd(X)\to \bderivedd(X),~\she\mapsto \she \tensor L
  \]
  is an equivalence (note that we don't need to the derive the tensor product, since tensoring with line bundles is exact).
  Then this can be regarded as the Fourier-Mukai transformation with kernel $\directimage{\Delta}L$.
  This is again basically the same as in i), but I'm not sure where we use that $L$ is a line bundle.
\end{enumerate}
\end{example}
\begin{prop}
  Let
  \[
  \Phi_{\she}\mc \bderivedd(X)\to \bderivedd(Y)
  \]
  be a Fourier-Mukai transformation with kernel $\she\in \bderivedd(X\times Y)$. Set
  \[
  \she^{\lderived}\defined
  \she^\vee\dtensor \inverseimage{q}\omega_Y[\dim(Y)]
  ~\text{and}~
  \she^{\rderived} \defined
  \she^\vee\dtensor \inverseimage{p}\omega_X[\dim(X)]
  \]
  Then we have adjunctions:
  \[
  \Phi_{\she^{\lderived}}\mc \bderivedd(Y)\leftrightarrows\bderivedd(X)\mcc \Phi_{\she}
  ~\text{and}~
  \Phi_{\she}\mc \bderivedd(X)\leftrightarrows\bderivedd(Y)\mcc \Phi_{\she^{\rderived}}
  \]
\end{prop}
\begin{proof}
  The proof can be found in \cite{hubible} and is an application of a lot of functorial isomorphisms (including Grothendieck-Verdier Duality).
  Note that the line he doesn't explain comes from the fact that both $\omega_X$ and $\omega_Y$ are actually invertible (I think), so tensoring with them is an equivalence.
\end{proof}
\subsection{Definition of Hochschild (Co)Homology for Varieties}
\begin{construction}\label{varities:product-on-ext}
  Let $\acat$ be an abelian category with derived category $\derivedd(\acat)$, and $A\in \acat$ an object.
  Then the shift functor gives rise to a product structure on the graded module
  \[
  \bigoplus_{i\in \zz}\hom_{\derivedd(\acat)}(A,A[i])
  \]
  induced by the maps
  \[
  \begin{tikzcd}
    \hom_{\derivedd(\acat)}(A,A[i])
    \tensor
    \hom_{\derivedd(\acat)}(A,A[j])
    \ar{d}[left]{\cong}[right]{\id\tensor [i]}
    \\
    \hom_{\derivedd(\acat)}(A,A[i])
    \tensor
    \hom_{\derivedd(\acat)}(A[i],A[i+j])
    \ar{d}[right]{\mathrm{composition}}
    \\
    \hom_{\derivedd(\acat)}(A,A[i+j])
  \end{tikzcd}
  \]
  This should be the same as the Yoneda-Product on the $\ext$-algebra.
\end{construction}
\begin{defn}
  Let $X$ be a smooth projective variety and denote by $\Delta$ the diagonal embedding $X\hookrightarrow X\times X$.
  Then the \emph{$i$-th Hochschild cohomology} of $X$ is defined as
  \[
  \hh^i(X)\defined \ext^i_{X\times X}(\directimage{\Delta}\ox,\directimage{\Delta}\ox).
  \]
  The \emph{graded Hochschild cohomology module} is then defined as
  \[
  \coh(X)\defined \bigoplus_{i\in \zz}\hh^i(X).
  \]
  In light of \cref{varities:product-on-ext}, this is actually a graded algebra.
  \glsadd{hochschild-cohomology-variety}
\end{defn}
\begin{rem}
  In the case that $X = \spec(A)$ is affine, this recovers our definition of the Hochschild cohomology of $A$ as
  \[
  \hh^i(A)\defined \ext_{A\tensor A}^i(A,A)
  \]
\end{rem}
The following definition seems to come a bit out of thin air, but we will motivate it later.
\begin{defn}
  Let $X$ be a smooth projective variety and denote by $\Delta$ the diagonal embedding $X\hookrightarrow X\times X$.
  Then the \emph{$i$-th Hochschild homology} of $X$ is defined as
  \begin{equation}\label{varieties:hochschild-original}
  \hh_i(X)\defined \ext_{X\times X}^i(\directimage{\Delta}\ox,\directimage{\Delta}\omega_X[\dim(X)]).
  \end{equation}
  \glsadd{hochschild-homology-variety}
  The \emph{graded Hochschild homology module} is then defined as
  \[
  \hoh(X)\defined \bigoplus_{i\in \zz}\hh_i(X).
  \]
\end{defn}
\begin{lem}\label{varities_defn:finite-dimensional}
  Let $X$ be a smooth projective variety.
  Then the Hochschild cohomology $\hh^i(X)$ of $X$ is concentrated in $[0,2\dim(X)]$ and the Hochschild homology $\hh_i(X)$ of $X$ is concentrated in $[-\dim(X), \dim(X)]$.
  Both the graded Hochschild homology module $\hoh(X)$ and the graded Hochschild cohomology module $\hoh(X)$ are finite-dimensional over $k$.
\end{lem}
\begin{proof}
  ...
\end{proof}
\begin{lem}
  The graded Hochschild homology module $\hoh(X)$ is a left module over the the graded Hochschild cohomology algebra $\coh(X)$.
\end{lem}
\begin{proof}
  Again, using the alternative definition of $\ext$ in the derived category, we can regard a Hochschild $i$-cocycle $\alpha\in \hh^i(X)$ as a map
  \[
    \alpha\mc \directimage{\Delta}\ox\to \directimage{\Delta}\ox[i]
  \]
  in $\bderivedd(X\times X)$ and a Hochschild $j$-cocycle $\beta\in \hh_j(X)$ as a map
  \[
  \beta\mc \directimage{\Delta}\ox\to \directimage{\Delta}\omega_X[\dim(X)+j]
  \]
  Using a shift by $i$, we obtain a map
  \[
  \begin{tikzcd}
    \directdelta\ox
    \ar[dashed]{rr}[above]{\alpha\cdot \beta}
    \ar{dr}[below left]{\alpha}
    &
    &
    \directdelta\omega_X[\dim(X)+j+i]
    \\
    &
    \directdelta\ox[i]
    \ar{ur}[below right]{\beta[i]}
    &
  \end{tikzcd}
  \]
\end{proof}
\begin{numtext}
  We want to give a second definition of the Hochschild homology of $X$ which is probably better motivated.
  For that, we recall that we had the $\tor$-interpretation of Hochschild homology as
  \[
  \hh_i(A)\defined \tor_i^{A^e}(A,A).
  \]
  Now given general $k$-modules $M$ and $N$, their
   $\tor$-groups can be computed as
  \[
  \tor_i(M,N) \cong \hlgy^i(M\dtensor_{\derivedd(k\modcat)}N)
  \]
  So one naive generalization would be to consider the homology of the complex
  \begin{equation}\label{varieties:hochschild-naive}
  \rderived\Gamma(\directdelta\ox\dtensor_{X\times X}\directdelta\ox),
  \end{equation}
  where $\rderived\Gamma$ is the right-derived functor of the global sections functor $\Gamma$.
  This seems to be motivated by the way Hochschild homology is defined for differential graded categories.
  \par
  In the lecture we had yet another definition ---
  we first considered the complex
  \begin{equation}\label{varieties_defn:second-hochschild}
  \shf \defined \rderived \directimage{p}\left(\directdelta\ox \dtensor_{X\times X}\directdelta \ox\right),
  \end{equation}
  which is an element of $\bderivedd(X)$.
  We then considered the complex
  $
  \rderived\Gamma(X;\shu)^\vee,
  $
  and claimed that its cohomology agrees with our original defintion of Hochschild homology in \eqref{varieties:hochschild-original}. Note that we can already know, by the definition of $\ext$ as right-derived functor, that Hochschild homology is given by the cohomology of the complex
  \[
  \rderived \hom_{X\times X}(\directimage{\Delta}\ox,\directdelta\omega_X[\dim X])
  \]
  As it turns out, all of them are equivalent!
  Before we can prove this, we need to enter the realm of Grothendieck and Serre Duality once again:
\end{numtext}
\begin{defn}
  Let $\acat$ be a $k$-linear category.
  A \emph{Serre functor} is a $k$-linear equivalence
  $
  S\mc \acat\to\acat
  $
  together with a collection of natural $k$-linear isomorphisms
  \[
  \eta_{A,B}\mc
  \hom_{\acat}(A,B)\isomorphism \hom_{\acat}(B,S(A))^\vee
  \]
  for all objects $A,B\in \acat$.
\end{defn}
\begin{prop}[Serre Duality]\label{varities_defn:serre-duality}
  Let $X$ be a smooth projective variety over a field $k$.
  Then the functor
  \[
  \bderivedd(X)\to \bderivedd(X),~
  \shf \mapsto \shf\dtensor \omega_X[\dim(X)]
  \]
  is a Serre functor.
\end{prop}
\begin{proof}
  We use the adjunction
  \[
  \rderived\directimage{f}\mc \bderivedd(X)\leftrightarrows\bderivedd(Y)\mcc \shriek{f}
  \]
  for this, applied to the structure morphism $f\mc X\to \spec(k)$ of the variety $X$.
  In that case we have $n\defined\dim(f) = \dim(X)$, the derived direct image is given by the derived global sections, i.e. $\rderived\directimage{f} = \rderived\Gamma$, and the functor $\shriek{f}$ applied to $\spec(k)$ is just the shifted canonical bundle, i.e. $\shriek{f}\spec(k) \cong \omega_X[n]$. Using the derived tensor-hom adjunction, we get for all $\she,\shf\in \bderivedd(X)$:
  \begin{align*}
    \hom_{\bderivedd(X)}(\she,\shf)^\vee
    &=
    \hom_{\bderivedd(\spec(k))}\left(\rderived\Gamma\left(\rderived\shhom\left(\she,\shf\right)\right),\spec(k)\right)
    \\
    &=
    \hom_{\bderivedd(\spec(k))}\left(\rderived\directimage{f}\left(\rderived\shhom\left(\she,\shf\right)\right),\spec(k)\right)
    \\
    &\cong
    \hom_{\bderivedd(X)}\left(\rderived\shhom\left(\she,\shf\right),\shriek{f}\spec(k)\right)
    \\
    &\cong
    \hom_{\bderivedd(X)}\left(\rderived\shhom\left(\she,\shf\right),\omega_X[n]\right)
    \\
    &
    \cong
    \hom_{\bderivedd(X)}\left(\shf,\she\dtensor\omega_X[n]\right)
  \end{align*}
  Taking duals again, the claim follows.
\end{proof}
\begin{prop}\label{varities_defn:equivalent-definitions-hoh}
  The defintions of Hochschild homology as in \eqref{varieties:hochschild-original}, \eqref{varieties:hochschild-naive} and \eqref{varieties_defn:second-hochschild} are all naturally isomorphic.
\end{prop}
\begin{proof}
  We first show that our ad-hoc defininition \eqref{varieties:hochschild-original} and the na\"{i}ve one \eqref{varieties:hochschild-naive} agree:
  For that we start with the following fact, which is a consequence of Grothendieck duality:
  \begin{fact}
    It holds that
    $\left(\directdelta\ox\right)^{\vee}
    \cong
    \directdelta\omega_X^{-1}[-\dim(X)]
    $.
  \end{fact}
  \begin{claimproof}
    By the definition of the dual, we have
    \begin{align*}
      \left(\directdelta\ox\right)^{\vee}
      &
      \defined
      \rderived\shhom_{X\times X}\left(\directdelta\ox,\oo_{X\times X}\right)
      \intertext{using Grothendieck-duality \cref{varities_defn:gvduality} in its full form and that $\directdelta$ is right exact, we get}
      &
      \cong
      \directdelta\rderived\shhom_X(\ox,\shriek{\Delta}\oo_{X\times X})
      \\
      &
      =
      \directdelta
      \rderived\shhom
      (\ox,\lderived\inverseimage{\Delta}(\oo_{X\times X})\dtensor \omega_{\Delta}[\dim(X)]).
      \intertext{If we use the identification $\omega_{X\times X}\cong \inverseimage{p_1}\omega_X\tensor \inverseimage{p_2}\omega_X$ (for an explanation of this in the affine case, see \cite{eisenbud_commutative_algebra}*{Prop. 16.5}) and the definition of $\omega_{\Delta}$, we first calculate}
      \omega_{\Delta}[\dim(X)]
      &
      =
      \omega_X \tensor \inverseimage{\Delta}\omega_{X\times X}^{-1}[-\dim(X)]
      \\
      &
      \cong
      \omega_X \tensor \inverseimage{\Delta}\left(\inverseimage{p_1}\omega_X^{-1}\tensor \inverseimage{p_2}\omega_X^{-1}\right)
      \\
      &
      \cong
      \omega_X^{-1}[-\dim(X)]
      \intertext{Inserting this in what we already had for the dual of $\directdelta\ox$, we get}
      (\directdelta\ox)^{\vee}
      &
      \cong
      \directdelta
      \rderived \shhom(\ox, \lderived\inverseimage{\Delta}(\oo_{X\times X})\dtensor \omega_X^{-1}[-\dim(X)])
      \intertext{Using the projection formula \eqref{varieties:projection-formula} one last time, this is isomorphic to }
      &
      \cong \directdelta\omega_X^{-1}[-\dim(X)]\dtensor_{\oo_{X\times X}}\oo_{X\times X}
      \\
      &
      \cong \directdelta\omega_X^{-1},
    \end{align*}
    which finishes the proof of this claim.
  \end{claimproof}
  We now get that
  \begin{align*}
  \rderived\Gamma(X\times X;\directdelta \ox\dtensor \directdelta\ox)
  &\cong
  \rderived \hom_{\bderivedd(X\times X)}(\oo_{X\times X},\directdelta\ox\dtensor\directdelta\ox)
  \\
  &
  \cong
  \rderived\hom_{\bderivedd(X\times X)}\left((\directdelta\ox)^{\vee},\directdelta\ox\right).
  \intertext{After taking cohomology and inserting what we obtained for the dual of $\directdelta\ox$, we finally get}
  \hh_{\bullet}(X)
  &
  =
  \ext^{\bullet}_{X\times X}\left(\directdelta\ox,\directdelta\omega_X[\dim(X)]\right)
  \\
  &
  \cong
  \ext^{\bullet}_{X\times X}\left(\directdelta(\omega_X^{-1}[\dim(X)]),\directdelta\ox\right)
  \\
  &
  \cong
  \ext^{\bullet}_{X\times X}\left((\directdelta\ox)^{\vee},\directdelta\ox\right)
  \\
  &
  \cong
  \hlgy^{\bullet}(\rderived\Gamma(X\times X;\directdelta\ox\tensor\directdelta\ox))
  \end{align*}
  Which shows that the ad-hoc defininition and the na\"{i}ve definition indeed agree.\par
  So we're left with showing that the na\"{i}ve definition and \eqref{varieties_defn:second-hochschild} also give the same for Hochschild homology:
  For the $\shf$ from \cref{varieties_defn:second-hochschild}, we have:
  \begin{align*}
  \rderived\Gamma(X;\shf)
  &
  =
  \rderived\hom_X(\ox,\shf)
  \\
  &
  =
  \rderived\hom_X
  (\ox, \rderived\directimage{p}(\directdelta\ox\dtensor\directdelta\ox))
  \intertext{Using the adjunction between $\lderived\inverseimage{p}$ and $\rderived\directimage{p}$ and the derived hom-tensor adjunction, we can write this as:}
  &\cong \rderived \hom_{X\times X}
  (\oo_{X\times X},\directdelta\ox\dtensor\directdelta\ox)
  \\
  &\cong
  \rderived \hom_{X\times X}
  ((\directdelta\ox)^\vee,\directdelta\ox)
  \\
  &
  \cong
  \rderived \hom_{X\times X}
  (\directdelta\omega_X^{-1}[-\dim(X)],\directdelta\ox)
  \intertext{We now apply Serre Duality (\cref{varities_defn:serre-duality}) for $\omega_{X\times X}[2\dim(X)]$ to write this as:}
  &\cong
  \rderived \hom_{X\times X}
  (\directdelta\ox, \directdelta\omega_X^{-1}\dtensor \omega_{X\times X}[\dim(X)])^{\vee}
  \\
  &
  \cong
  \rderived \hom_{X\times X}
  (\directdelta\ox, \directdelta\omega_X^{-1}\dtensor (\inverseimage{p_1}\omega_X\dtensor\inverseimage{p_2}\omega_X)[\dim(X)])^{\vee}
  \intertext{The projection formula allows us to cancel one of the $\omega_X$-terms with the $\omega_X^{-1}$, so this reads as}
  &
  \cong
  \rderived\hom_{X\times X}
  (\directdelta\ox, \directdelta\omega_X[\dim(X)])^{\vee}.
  \end{align*}
  After taking duals again and identitifying the double dual on the right with the original complex, the claim finally follows.
\end{proof}
\subsection{Hochschild (Co)Homology of $\pp^n$}
\begin{numtext}
  In this section, we want to compute the Hochschild (co)homology of the projective space $\pp^n$ over $k$, using an explicit locally free resolution of the diagonal which is called \emph{Beilinson resolution.}
  In the next section, I'll try to use some more general descent theorems for Hochschild (co)homology, and we will see that they indeed produce the same result.
  I don't know how they relate to each other, but it feels like they should.
\end{numtext}
\begin{defn}
  Let $\shf,\shg \in \qcohcat(\pp^n)$ be two quasi coherent sheaves on $\pp^n$; denote by $p,q\mc \pp^n\times\pp^n\to \pp^n$ the projection onto the first respective the second factor.
  Then the \emph{exterior tensor product} of $\shf$ and $\shg$ is defined as
  \[
  \shf \boxtimes \shg \defined \inverseimage{p}\shf\tensor_{\pp^n\times \pp^n}\inverseimage{q}\shg,
  \]
  which is a sheaf on $\pp^n\times \pp^n$.
\end{defn}
\begin{nonumnotation}
  As is (apparently) customary, we use the following notation when we're talking about the projective space:
  We set $\oo\defined \oo_{\pp^n}$ for the structure sheaf, and $\Omega\defined \kahl{\pp^n}{k}$ for the sheaf of differentials.
  For a general sheaf $\shf\in \qcohcat(\pp^n)$, we denote by $\shf(n)$ it's \emph{$n$-th twist}, i.e. the result of tensoring with the twisting sheaf $\oo(n)$.
  Denote by $\Delta\mc \pp^n\hookrightarrow \pp^n\times \pp^n$ the diagonal embedding.
  Then we also write $\ood$ for $\directdelta\oo$.
\end{nonumnotation}
\subsubsection{Using Beilinson's resolution}
\begin{prop}[Beilinson's resolution]
  There is a locally free resolution of $\ood$ of the form:
  \[
  \begin{tikzcd}[cramped, row sep = small, column sep = small]
    0
    \ar{r}
    &
    \exterior^n(\oo(-1)\boxtimes \Omega(1))
    \ar{r}
    &
    \exterior^{n-1}(\oo(-1)\boxtimes \Omega(1))
    \ar{r}
    \ar[phantom]{d}[name = X, anchor = center]{}
    &
    \ldots
    \ar[rounded corners,
          to path={ -- ([xshift=2ex]\tikztostart.east)
                    |- (X.center) \tikztonodes
                    -| ([xshift=-2ex]\tikztotarget.west)
                    -- (\tikztotarget)}]{dlll}[at end]{}
    \\
    \ldots
    \ar{r}
    &
    \oo(-1)\boxtimes\Omega(1)
    \ar{r}
    &
    \oo_{\pp^n\times\pp^n}
    \ar{r}
    &
    \oo_{\Delta}
    \ar{r}
    &
    0
  \end{tikzcd}
  \]
\end{prop}
\begin{proof}
  For now, I can't prove this.
  A referene is \cite{hubible}*{Lemma 8.27}.
  This can also be shown in the more general setup of grassmannians,see \cite{kapranov-grassmannian}.
\end{proof}
\begin{example}\label{varieties-defn:pn-example}
  We can use this sequence to calculate both the Hochschild cohomology and homology of $\pp^n$. Denote the Beilinson resolution by $\shbb$.
  Then we have for the cohomology:
  \begin{align*}
    \hh(X)^i
    &
    =
    \ext^i_{X\times X}(\oo_{\Delta},\oo_{\Delta})
    &
    \\
    &
    =
    \ext^i_{X\times X}(\shbb,\oo_{\Delta})
    &
    \text{($\shbb\simeq \oo_{\Delta}$)}
    \\
    &
    \cong
    \ext^i_{X}(\inverseimage{\Delta}\shbb,\ox)
    &
    \text{($\inverseimage{\Delta}\dashv\directdelta$)}
    \\
    &
    =
    \hlgy^i(X;(\inverseimage{\Delta}\shbb)^\vee)
    &
    \text{(definition of $\ext^i_X$)}
    \intertext{Similary for the homology, we get, using the na\"{i}ve definition:}
    \hh_i(X)
    &
    =
    \hlgy^i(\rderived\Gamma(\oo_{\Delta}\dtensor\oo_{\Delta}))
    &
    \\
    &
    =
    \hlgy^i(\rderived\Gamma(\shbb\tensor\oo_{\Delta}))
    &
    \text{($\shbb\simeq \oo_{\Delta}$)}
    \\
    &
    \cong
    \hlgy^i(\rderived\Gamma(\inverseimage{\Delta}\shbb\dtensor\ox))
    &
    \\
    &
    =
    \hlgy^i(X;\inverseimage{\Delta}\shbb)
    &
    \text{(definition of $\dtensor$)}
  \end{align*}
  To calculate these cohomologies, we first note that we can re-write the $i$-th term of $\shbb$ as:
  \begin{align*}
  \exterior^i(\oo(-1)\boxtimes\Omega(1))&\cong \oo(-i)\boxtimes\Omega^i(i),
  \intertext{and so in particular,}
  \inverseimage{\Delta}\left(\exterior^i(\oo(-1)\boxtimes\Omega(1))\right)
  &
  \cong
  \Omega^i
  \end{align*}
  holds.
  We also have that the differentials of $\inverseimage{\Delta}\shbb$ vanish,
  and so we can write
  \[
  \inverseimage{\Delta}\shbb = \bigoplus_{i=0}^n \Omega^i[i],
  \]
  where $\Omega^i[i]$ is the complex with $\Omega^i$ concentrated in degree $i$.
  With this description, we get for Hochschild homology
  \begin{align*}
    \hh_j(\pp^n)
    &
    \cong
    \hlgy^j(X;\inverseimage{\Delta}\shbb)
    \\
    &
    =
    \hlgy^j\left(X;\bigoplus_{i=0}^n \Omega^i[i]\right)
    \\
    &
    =
    \bigoplus_{i=0}^n\hlgy^j\left(X;\Omega^i[i]\right)
    \\
    &
    =
    \bigoplus_{i=0}^n\hlgy^{j+i}\left(X;\Omega^i\right)
    \\
    &
    \cong
    \begin{cases}
      k^{\oplus n},&\text{if }j=0
      \\
      0,
      &
      \text{otherwise.}
    \end{cases}
    \intertext{where the last line follows the fact that $\hlgy^r(X;\Omega^s) \neq 0$ only for $r=s$, in which case it is isomorphic to $k$.
    For Hochschild cohomology, or more precisley the dual of $\Omega^i$, we don't have such an identity, and so we only get as far as}
    \hh^j(\pp^n)
    &
    \cong \bigoplus_{i=0}^n\hlgy^{j+i}(X;(\Omega^i)^\vee)
  \end{align*}
\end{example}
\subsubsection{Using exceptional sequences}
\begin{numtext}
  One application of Beilinson's resolution is proving the existence of \emph{Beilinson's spectral sequence} (\cref{varieties_defn:beilinson-spectral}).
  This leads to a description of the triangulated structure on $\bderivedd(\pp^n)$ in terms of certain ``indecomposable generators''.
  In \cite{kuznetsov-semiorthogonal}, Kuznetsov showed how to calculate Hochschild (co)homology in this setup, by associating Fourier-Mukai kernels to the projections onto the components of these decomposition, and then relating the Hochschild (co)homology of the variety whose derived category we decomposed to the Hochschild (co)homology of these kernels.
\end{numtext}
\begin{prop}\label{varieties_defn:beilinson-spectral}
  For any coherent sheaf $\shf$ on $\pp^n$ there exists a natural spectral sequence:
  \begin{equation}
    E_1^{r,s}\defined
    \hlgy^s(\pp^n;\shf(r))\tensor \Omega^{-r}(-r) \Rightarrow
    E^{r+s}
    =
    \begin{cases}
      \shf
      &
      \text{if }r+s = 0
      \\
      0
      &
      \text{otherwise.}
    \end{cases}
    \end{equation}
\end{prop}
\begin{proof}
  This can be found in \cite{hubible}*{Proposition 8.28}.
\end{proof}
\begin{defn}
  An object $E\in \dcat$ in a $k$-linear triangulated category $\dcat$ is called \emph{exceptional} if
  \[
  \hom_{\dcat}(E,E[\ell])
  =
  \begin{cases}
    k&\text{if }\ell = 0
    \\
    0&\text{otherwise.}
  \end{cases}
  \]
  A collection of exceptional objects $E_1,\ldots,E_n$ is called an \emph{exceptional sequence} if $\hom(E_i,E_j[\ell]) = 0$ for all $i>j$ and all $\ell$.
  An exceptional sequence is called \emph{full} if $\dcat$ is generated by the $E_i$ as a triangulated category.
\end{defn}
\begin{defn}
  \leavevmode
  \begin{enumerate}
    \item Let $A\in \dcat$ be an object in a $k$-linear triangulated category $\dcat$.
    Then
    \[
    A^{\perp}\defined
    \lset B\in \dcat\ssp \hom(A,B[i]) = 0\text{ for all }i\in \zz\rset
    \]
    is the \emph{orthogonal complement} of $A$.
    Similarly, for an admissible subcategory $\dcat'\sse \dcat$, the \emph{orthogonal complement} of $\dcat'$ is the full subcategory $\dcat^{\perp}$ with objects given by
    \[
    \lset C\in \dcat\ssp \hom(B,C) = 0\text{ for all }B\in \dcat'\rset.
    \]
    \glsadd{orthogonal-complement}
    \item A sequence of full admissible triangulated subcategories $\dcat_1,\ldots,\dcat_n\sse\dcat$ is called \emph{semi-orthogonal} if for all $i>j$ it holds that $\dcat_j\sse \dcat_i^{\perp}$.
    Such a sequence defines a \emph{semi-orthogonal decomposition} of $\dcat$ if $\dcat$ is generated as a triangulated category by the $\dcat_i$.
  \end{enumerate}
\end{defn}
\begin{lem}
  Let $E_1,\ldots,E_n$ be a an exceptional sequence in $\dcat$.
  then the admissible triangulated subcategories
  \[
  \dcat_1\defined \genby{E_1},\ldots,\dcat_n\defined\genby{E_n}
  \]
  form a semi-orthogonal sequence.
\end{lem}
\begin{prop}[\cite{hubible}*{Corollary 8.29}]
  Any sequence of line bundles of the form
  \[
  \oo(a),\oo(a+1),\ldots,\oo(a+n)
  \]
  on $\pp^n$ defineds a full exceptional sequence in $\bderivedd(\pp^n)$.
\end{prop}
\begin{proof}
  I want to at least try to understand this proof\footnote{This is my first ever spectral sequence argument, so I'm a bit excited...}, so bear with me while I to write down a worse version of what can be found in Huybrechts' book:
  We have that any line bundle in $\oo(i)$ is an exceptional object of $\bderivedd(\pp^n)$, since
  \[
  \ext^{\ell}(\oo(i),\oo(i)) \cong \hlgy^{\ell}(\pp^n,\oo)
  =
  \begin{cases}
    k&\text{if }\ell = 0
    \\
    0
    &
    \text{otherwise.}
  \end{cases}
  \]
  We also have, for all $-n\leq j-i<0$
  \[
  \ext^{\ell}(\oo(i),\oo(j))
  \cong
  \hlgy^{\ell}(\pp^n,\oo(j-i))
  =
  0,
  \]
  and so $\oo(a),\ldots,\oo(a+n)$ is indeed an exceptional sequence.\par
  We're left with showing that the exceptional sequence is full.
  For that, it suffices to show that any complex $\shf$ in $\genby{\oo(a),\ldots,\oo(a+n)}^{\perp}$ is trivial.
  In general, this needs some argument from the proof of Beilinson's spectral sequence, but if $\shf$ comes from an honest sheaf, we can use the sequence directly --- so let's assume this for the moment.
  We know that $\Omega^{-r}(-r)$ is non-trivial only for $-n \leq r \leq 0$.
  Inserting this into the $E_1$-page of \cref{varieties_defn:beilinson-spectral}, we get for the twist $\shf(-a)$:
  \[
    E_1^{r,s}
    =
    \hlgy^s(\pp^n;\shf(-a)(r)) \tensor \omega^{-r}(-r)
    =
    \ext^s(\oo(i),\shf)\tensor\Omega^{-r}(-r)
  \]
  where we set $i\defined a -r$.
  In particular, $E_1^{r,s}$ can be non-zero only for $-n\leq r \leq 0$.
  If now $\shf$ is orthogonal to all the $\oo(i)$ for $1\leq i \leq a+n$, i.e. $\ext^s(\oo(i),\shf) = 0$, then the remaining possible non-zero $E_1^{r,s}$ are in fact also zero. and hence the limit of the spectral sequence is too.
  This gives $\shf\cong \shf(-a) = 0$.
  For the proof in the general setup, we have to refer the reader to Huybrechts book for the time being.
\end{proof}
\begin{numtext}
  We now describe how to represent the projection functors from this semi-orthogonal decomposition by certain Fourier-Mukai kernels, using the language and results of \cite{kuznetsov_2011}.
\end{numtext}
\begin{lem}[\cite{kuznetsov_2011}*{Lemma 2.3}]\label{varieties_defn:semiorthogonal-filtration}
  Let $\genby{\acat_1,\ldots,\acat_n}$ be a semiorthogonal decomposition of $\dcat$.
  Then for every object $T\in \dcat$, there is a chain of morphisms
  \[0 = D_n\to D_{n-1}\to \ldots D_1 \to D_0 = D
  \]
  such that the cone of the morphism $D_{k}\to D_{k-1}$ is contained in $\dcat_k$ for all $1\leq k \leq m$, or, equivalently, there are $A_k\in \acat_k$ and a diagram of the form
  \[
  \begin{tikzcd}[column sep = tiny, cramped]
    0
    \ar[equal]{r}
    &
    D_m
    \ar{rr}
    &
    &
    D_{m-1}
    \ar{ld}
    \ar{rr}
    &
    &
    \ldots
    \ar{rr}
    &
    &
    D_2
    \ar{rr}
    &
    &
    D_1
    \ar{ld}
    \ar{rr}
    &
    &
    D_0
    \ar{ld}
    \ar[equal]{r}
    &
    D
    \\
    &
    &
    A_m
    \ar[dashed]{ul}
    &
    &
    &
    \ldots
    &
    &
    &
    A_2
    \ar[dashed]{ul}
    &
    &
    A_1
    \ar[dashed]{ul}
    &
    &
  \end{tikzcd},
  \]
  where we use the dashed arrows to indicate a shift by one.
  We call such a diagram a \emph{filtration} (with respect to the $\acat_i$) of $D$.
  For every $D$, the association of a filtration is unique and functorial.
  Conversely, if we're given a collection of full triangulated subcategories $\acat_1,\ldots,\acat_n$ such that $\acat_i\sse\acat_j^{\perp}$ holds whenever $i<j$ and every object $D$ has a filtration with respect to the $\acat_i$, then they define a semiorthogonal decomoposition of $\dcat$.
\end{lem}
\begin{construction}
  In the case of \cref{varieties_defn:semiorthogonal-filtration}, we obtain for every $1\leq k \leq n$ a well-defined functor $\alpha_k\mc \dcat\to \dcat$, which associates to an object $D\in \dcat$ the object $A_k\in \acat_k$ of the filtration of $D$.
  We call this functor the \emph{$k$-th projection functor.}
\end{construction}
